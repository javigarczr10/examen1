/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen1;

import java.util.logging.Logger;

/**
 *
 * @author safoe
 */
public abstract class Empleado {
    protected int numEmpleado;
    protected String nombre;
    protected String domicilio; 
    protected Contrato Contrato;

    public Empleado() {
        this.numEmpleado=0;
        this.nombre="";
        this.domicilio="";
        this.Contrato= new Contrato();
    }
    
    

    public Empleado(int numEmpleado, String nombre, String domicilio, Contrato Contrato) {
        this.numEmpleado = numEmpleado;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.Contrato = Contrato;
    }

    public int getNumEmpleado() {
        return numEmpleado;
    }

    public void setNumEmpleado(int numEmpleado) {
        this.numEmpleado = numEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public Contrato getContrato() {
        return Contrato;
    }

    public void setContrato(Contrato Contrato) {
        this.Contrato = Contrato;
    }
    
    
    
    public float calcularTotal(){
        return 12;
    }
    
    
    
    
}
