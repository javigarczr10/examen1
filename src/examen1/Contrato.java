/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen1;

/**
 *
 * @author safoe
 */
public class Contrato {
    private int claveContrato;
    private String puesto;
    private float impuesto;

    public Contrato() {
        this.claveContrato=0;
        this.impuesto=0.0f;
        this.puesto="";
    }

    public Contrato(int claveContrato, String puesto, float impuesto) {
        this.claveContrato = claveContrato;
        this.puesto = puesto;
        this.impuesto = impuesto;
    }

    public int getClaveContrato() {
        return claveContrato;
    }

    public void setClaveContrato(int claveContrato) {
        this.claveContrato = claveContrato;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public float getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(float impuesto) {
        this.impuesto = impuesto;
    }
    
    
    
}
