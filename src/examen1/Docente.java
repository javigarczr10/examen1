/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen1;

/**
 *
 * @author safoe
 */
public class Docente extends Empleado{
    private int nivel;
    private float pagosHora;
    private int horasLaboradas;

    public Docente() {
        this.nivel=0;
        this.pagosHora=0.0f;
        this.horasLaboradas=0;
    }

    public Docente(int nivel, float pagosHora, int horasLaboradas, int numEmpleado, String nombre, String domicilio, examen1.Contrato Contrato) {
        super(numEmpleado, nombre, domicilio, Contrato);
        this.nivel = nivel;
        this.pagosHora = pagosHora;
        this.horasLaboradas = horasLaboradas;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getPagosHora() {
        return pagosHora;
    }

    public void setPagosHora(float pagosHora) {
        this.pagosHora = pagosHora;
    }

    public int getHorasLaboradas() {
        return horasLaboradas;
    }

    public void setHorasLaboradas(int horasLaboradas) {
        this.horasLaboradas = horasLaboradas;
    }

    public int getNumEmpleado() {
        return numEmpleado;
    }

    public void setNumEmpleado(int numEmpleado) {
        this.numEmpleado = numEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public examen1.Contrato getContrato() {
        return Contrato;
    }

    public void setContrato(examen1.Contrato Contrato) {
        this.Contrato = Contrato;
    }
    

    public float calcularTotal(int t) {
       float f=0;
       f=this.pagosHora*this.horasLaboradas;
       if(t==1)
           f=f + (f*0.35f);
       if(t==2)
           f=f + (f*0.40f);
       if(t==3)
           f=f + (f*0.50f);
       else
           return f;
      return f;
    }


    public float calcularImpuesto() {
        return (this.pagosHora*this.horasLaboradas) * (this.Contrato.getImpuesto()/100);
    }
    
}
    
    

